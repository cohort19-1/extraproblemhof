
// 1. Find all Web Developers


const webDevelopersOnly = function(data){
    webDevolopers =data.filter(element => {
        job = element.job;
        if(job.indexOf('Web Developer') != -1){
            return true;
        }
    })
    return{
        file : webDevolopers,
        filename: "webDevelopers"
    }
}



// 2. Convert all the salary values into proper numbers instead of strings


const salaryInNumber = function(data){
    data.forEach(element => {
        salary = (element.salary).split('');
        tempSalaryStr = '';
        salary.forEach(item=>{
            if(item ==='$'){
            }
            else if(typeof parseInt(item) === 'number' || item ==='.'){
                tempSalaryStr += item;
            }
        })
        element.salary = parseFloat(tempSalaryStr);
    });
    return{
        file : data,
        filename: "salaryInNumber"
    }
}

// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

const salaryFactorOf10000 = function(data){
    data.forEach(element => {
        salary = (element.salary).split('');
        tempSalaryStr = '';
        salary.forEach(item=>{
            if(item ==='$'){
            }
            else if(typeof parseInt(item) === 'number' || item ==='.'){
                tempSalaryStr += item;
            }
        })
        element.CorrectedSalary = parseFloat(tempSalaryStr)*10000;
    });
    return{
        file : data,
        filename: "correctedSalary"
    }
}


//4. Find the sum of all salaries

const sumSalary = function(data){
    data.forEach(element => {
        salary = (element.salary).split('');
        tempSalaryStr = '';
        salary.forEach(item=>{
            if(item ==='$'){
            }
            else if(typeof parseInt(item) === 'number' || item ==='.'){
                tempSalaryStr += item;
            }
        })
        element.salary = parseFloat(tempSalaryStr);
    });

    let salarySum = {totalsalary : data.reduce((sum,element)=>{
        return sum + element.salary;
    },0)}
    return{
        file : salarySum,
        filename: "sumOfSalary"
    }
}


// 5. Find the sum of all salaries based on country using only HOF method

const salaryCountryWise = function(data){

    let salaryBasedOnCountry = {}
    data.forEach(element=>{
        salary = parseFloat(element.salary.slice(1,element.salary.length));
        let location = element.location;
        if(!salaryBasedOnCountry[location]){
            salaryBasedOnCountry[location] = salary;
        }
        else{
            salaryBasedOnCountry[location] += salary;
        }
    })
    return{
        file : salaryBasedOnCountry,
        filename: "salaryBasedOnCountry"
    }
}




//6. Find the average salary based on country using only HOF method

const averageSalary = function(data){
    salaryCount = {};
    avgSalary={};
    data.forEach(element => {
        salary = (element.salary).split('');
        tempSalaryStr = '';
        salary.forEach(item=>{
            if(item ==='$'){
            }
            else if(typeof parseInt(item) === 'number' || item ==='.'){
                tempSalaryStr += item;
            }
        })
        element.salary = parseFloat(tempSalaryStr);
        salaryInDigits = element.salary;
        country = element.location;
        if(!salaryCount[country]){
            salaryCount[country]={};
            salaryCount[country].salary = salaryInDigits;
            salaryCount[country].count = 1;
        }
        else{
            salaryCount[country].salary += salaryInDigits;
            salaryCount[country].count += 1;
        }
        totalSalary = salaryCount[country].salary;
        totalTimes = salaryCount[country].count; 
        average= totalSalary/totalTimes;

        avgSalary[country]=average;
    });
    return{
        file : avgSalary,
        filename: "averageSalaryBasedOnCountry"
    }
}


module.exports.webDevelopersOnly = webDevelopersOnly;
module.exports.salaryInNumber = salaryInNumber;
module.exports.salaryFactorOf10000 = salaryFactorOf10000;
module.exports.sumSalary = sumSalary;
module.exports.salaryCountryWise = salaryCountryWise;
module.exports.averageSalary = averageSalary;

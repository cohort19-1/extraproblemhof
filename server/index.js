const fs = require('fs');
const path = require('path');


const problemFunction = require('./problem')

const filePath = path.resolve(`${__dirname}`,'../jsonDataFile.json')

const precticeData = function(inputFunction){
    fs.readFile(filePath,'utf-8',(err,data)=>{
        if(err){
            console.log(err);
        }
        else{
            jsonData = JSON.parse(data);
            let result = inputFunction(jsonData);
            outputFile  = JSON.stringify(result.file,null,2);
            outputFilePath = path.resolve(`${__dirname}`,`../output/${result.filename}.json`);
        }
        fs.writeFile(outputFilePath,outputFile,(err)=>{
            if(err){
                console.log(err);
            }
            else{
                console.log("saved");
            }
        })
    })
}





precticeData(problemFunction.webDevelopersOnly);
precticeData(problemFunction.salaryInNumber);
precticeData(problemFunction.salaryFactorOf10000);
precticeData(problemFunction.sumSalary);
precticeData(problemFunction.salaryCountryWise);
precticeData(problemFunction.averageSalary);
